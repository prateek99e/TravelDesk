﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TravelDesk.Context;
using TravelDesk.ViewModels;
using TravelDeskNst.Models;

namespace TravelDesk.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        TravelDeskDbContext _context;
        IConfiguration _configuration;

        //public ClaimsIdentity? FirstName { get; private set; }

        public LoginController(TravelDeskDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }
        [HttpPost]
        public IActionResult Login(LoginViewModel user)
        {
            IActionResult response = Unauthorized();
            var obj = Authenticate(user);

            CommonTypeRef c = _context.CommonTypes.Where(x => x.Id == obj.RoleId).FirstOrDefault();
            user.RoleName = c.Value;



            if (obj != null)
            {
                var tokenString = GenerateJSONWebToken(obj.Id, user.Email, user.RoleName
                    , obj.FirstName, obj.MiddleName, obj.LastName);
                response = Ok(new { token = tokenString });
            }
            return response;
        }
        private string GenerateJSONWebToken(int id, string email, string roleName,
           string firstName, string middleName, string lastName)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim("Id", id.ToString()));
            claims.Add(new Claim("Email", email));
            claims.Add(new Claim("Role", roleName.ToString()));
            claims.Add(new Claim("Name", $"{firstName} {middleName} {lastName}"));


     
            var securityKey = new
                 SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));



            //var securityKey = new SymmetricSecurityKey(_configuration["Jwt:key"]);



            var credentials = new SigningCredentials(securityKey,
            SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
           _configuration["Jwt:Issuer"],
            claims,
expires: DateTime.Now.AddMinutes(120),
signingCredentials: credentials);
            return new JwtSecurityTokenHandler().WriteToken(token);



        }
        private User Authenticate(LoginViewModel user)
        {
            User obj = _context.Users.FirstOrDefault(x => x.Email == user.Email
&& x.Password == user.Password);



            return obj;
        }



    }
}
